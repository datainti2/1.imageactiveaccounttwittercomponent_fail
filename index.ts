import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleComponent } from './src/sample.component';
import { SampleDirective } from './src/sample.directive';
import { SamplePipe } from './src/sample.pipe';
import { SampleService } from './src/sample.service';
import { MaterialModule } from '@angular/material';
import { WidgetBaseComponent } from './src/widget-base/widget-base.component';

export * from './src/sample.component';
export * from './src/sample.directive';
export * from './src/sample.pipe';
export * from './src/sample.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    SampleComponent,
    SampleDirective,
    SamplePipe,
    WidgetBaseComponent
  ],
  exports: [
    SampleComponent,
    SampleDirective,
    SamplePipe
  ]
})
export class ImgActvTwitter {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ImgActvTwitter,
      providers: [SampleService]
    };
  }
}
