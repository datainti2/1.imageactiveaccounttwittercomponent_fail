import { Component } from '@angular/core';

@Component({
  selector: 'app-widget-base',
  template: '<div class="col-md-10 col-md-offset-1"><md-grid-list cols="6"><md-grid-tile *ngFor="let img of images"><img src="{{img.image}}" class="img-rounded" alt="Cinque Terre" width="154" height="136"></md-grid-tile></md-grid-list></div>',
  styleUrls: ['./widget-base.component.css']
})
export class WidgetBaseComponent{
   public images: Array<any> = [
    {"image":"../assets/img/download1.jpg"}, 
    {"image":"../assets/img/download.jpg"},
    {"image":"../assets/img/images1.jpg"},
    {"image":"../assets/img/images2.jpg"},
    {"image":"../assets/img/images3.jpg"},
    {"image":"../assets/img/images.jpg"},
    {"image":"../assets/img/noimg.png"}, 
    {"image":"../assets/img/anonymous.png"},
    {"image":"../assets/img/v-twitter-3.jpg"},
    {"image":"../assets/img/noimg.png"}];
}
