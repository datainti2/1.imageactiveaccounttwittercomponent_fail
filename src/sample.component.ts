import { Component } from '@angular/core';

@Component({
  selector: 'sample-component',
  template: `<app-widget-base></app-widget-base>`
})
export class SampleComponent {

  constructor() {
  }

}
